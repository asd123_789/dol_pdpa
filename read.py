from tabnanny import check
import pandas as pd
import csv
import xlrd
import numpy as np
import random
import subprocess as sub
import random
import string
import time
import mysql.connector





def read_name_file(directory):
    a = directory
    p = sub.Popen((a,), stdout=sub.PIPE, shell=True)

    file_sum = []
    for row in iter(p.stdout.readline, b''):
        file_name = []
        # x is file
        
        x = row.rstrip()
        z = str(x).split(',')
        # y is IP Adresse
        y = z[0].replace("b'", "error404'")
        f = str(y).split('error404')


        mvf = f[1]
        sp = str(mvf).split(' ')
        filtered_list = []
        for ele in sp:
            if ele != '':
                ele = ele.replace("'", "")
                filtered_list.append(ele)
        if len(filtered_list) > 3:
            file_name.append(filtered_list[8])
            file_name.append(filtered_list[2])
            file_name.append(filtered_list[4])
            file_sum.append(file_name)
    return(file_sum)


def read_excel(file_name):
    check_columns = df = pd.read_excel('/var/pdpa/ftp/'+file_name[0],dtype=str).columns
    df = pd.read_excel('/var/pdpa/ftp/'+file_name[0],converters={'#id_card_number_xxx':str})
    wb = xlrd.open_workbook('/var/pdpa/ftp/'+file_name[0])
    sheet = wb.sheet_by_index(0)
    #CHECK ID IMPORT
    mycursor = mydb.cursor()
    sql = "SELECT * FROM TB_TR_IMPORT as im WHERE im.username = %s"
    adr = (file_name[1], )
    mycursor.execute(sql, adr)
    myresult = mycursor.fetchall()
    for x in myresult:
        id_import = x[0]
    #RANDOM
    letters = string.ascii_lowercase
    randomtext = ''.join(random.choice(letters) for i in range(20))
    

    
    title = []
    head_ex = []
    mycursor = mydb.cursor()
    mycursor.execute("SELECT * FROM TB_TR_PDPA_DATA")
    myresult = mycursor.fetchall()
    for x in myresult:
        title.append(x[0])
        head_ex.append(x[2])
    gg = []
    for check_l in check_columns:
        gg.append(check_l)

    
    if head_ex == gg:
        
        mycursor = mydb.cursor()
        sql = "INSERT INTO `TB_TR_IMPORT_FILE` (`id`, `name`, `date`, `import_id`, `rname`, `status`) VALUES (NULL, %s, %s, %s, %s, %s)"
        val = ( file_name[0], date, id_import, randomtext,0)
        mycursor.execute(sql, val)
        id_db = mycursor.lastrowid
        mydb.commit()

        for i in range(1, len(df)):
            sum = []
            for r in range(0, len(df.columns)):

                a = sheet.cell_value(i, r)
                print(a)
                sum.append(a)
                mycursor = mydb.cursor()
                sql = "INSERT INTO `TB_TR_IMPORT_DATA` (`id`, `columns`, `rows`, `value`, `type`, `source`, `date`, `doc_pdpa_data_id`, `import_file_id`) VALUES (NULL, %s, %s, %s, %s, %s, %s, %s, %s)"
                val = ( r, i, a, 'ftp',file_name[0], date, title[r], id_db)
                mycursor.execute(sql, val)
                mydb.commit()
        sub.call('mv /var/pdpa/ftp/'+file_name[0]+ ' /var/sum_file/'+randomtext+'.xlsx', shell=True)
        print('True')
        print(file_name[0], date, id_import, randomtext,0)
    else:
        print('false')

    
def read_csv(file_name):

    with open('/var/pdpa/ftp/'+file_name[0], 'r') as file:
        reader = csv.reader(file)
        df = pd.read_csv('/var/pdpa/ftp/'+file_name[0])
        list_of_column_names = list(df.columns)
        #RANDOM
        letters = string.ascii_lowercase
        randomtext = ''.join(random.choice(letters) for i in range(20))
        #CHECK ID IMPORT
        mycursor = mydb.cursor()
        sql = "SELECT * FROM TB_TR_IMPORT as im WHERE im.username = %s"
        adr = (file_name[1], )
        mycursor.execute(sql, adr)
        myresult = mycursor.fetchall()
        for x in myresult:
            id_import = x[0]
        

        
        title = []
        head_cs = []
        mycursor = mydb.cursor()
        mycursor.execute("SELECT * FROM TB_TR_PDPA_DATA")
        myresult = mycursor.fetchall()
        for x in myresult:
            title.append(x[0])
            head_cs.append(x[2])

        if head_cs == list_of_column_names:
            mycursor = mydb.cursor()
            sql = "INSERT INTO `TB_TR_IMPORT_FILE` (`id`, `name`, `date`, `import_id`, `rname`, `status`) VALUES (NULL, %s, %s, %s, %s, %s)"
            val = ( file_name[0], date, id_import, randomtext,0)
            mycursor.execute(sql, val)
            id_db = mycursor.lastrowid
            mydb.commit()

            data_csv = []
            for cs in reader:
                data_csv.append(cs)
            count = []
            for n in np.shape(data_csv):
                count.append(n)

            columns = int(count[0])
            row = int(count[1])
            for i in range(1, columns):
                sum = []
                for r in range(0, row):
                    a = data_csv[i][r]
                    sum.append(a)
                    mycursor = mydb.cursor()
                    sql = "INSERT INTO `TB_TR_IMPORT_DATA` (`id`, `columns`, `rows`, `value`, `type`, `source`, `date`, `doc_pdpa_data_id`, `import_file_id`) VALUES (NULL, %s, %s, %s, %s, %s, %s, %s, %s)"
                    val = ( r, i, a, 'ftp',file_name[0], date, title[r], id_db)
                    mycursor.execute(sql, val)
                    mydb.commit()
            sub.call('mv /var/pdpa/ftp/'+file_name[0]+ ' /var/sum_file/'+randomtext+'.csv', shell=True)
            print('True')
            print(file_name[0], date, id_import, randomtext,0)
        else:
            print('false')

def read_txt(file_name):
    file_name_old = file_name[0]
    retxt = file_name[0].replace("txt", "csv")
    sub.call('mv /var/pdpa/ftp/'+file_name[0]+ ' /var/pdpa/ftp/'+ retxt, shell=True)
    with open('/var/pdpa/ftp/'+retxt, 'r') as file:
        reader = csv.reader(file)
        df = pd.read_csv('/var/pdpa/ftp/'+retxt)
        list_of_column_names = list(df.columns)
        #RANDOM
        letters = string.ascii_lowercase
        randomtext = ''.join(random.choice(letters) for i in range(20))
        #CHECK ID IMPORT
        mycursor = mydb.cursor()
        sql = "SELECT * FROM TB_TR_IMPORT as im WHERE im.username = %s"
        adr = (file_name[1], )
        mycursor.execute(sql, adr)
        myresult = mycursor.fetchall()
        for x in myresult:
            id_import = x[0]
        

        id_db = mycursor.lastrowid
        title = []
        head_cs = []
        mycursor = mydb.cursor()
        mycursor.execute("SELECT * FROM TB_TR_PDPA_DATA")
        myresult = mycursor.fetchall()
        for x in myresult:
            title.append(x[0])
            head_cs.append(x[2])

        if head_cs == list_of_column_names:
            mycursor = mydb.cursor()
            sql = "INSERT INTO `TB_TR_IMPORT_FILE` (`id`, `name`, `date`, `import_id`, `rname`, `status`) VALUES (NULL, %s, %s, %s, %s, %s)"
            val = ( file_name_old, date, id_import, randomtext,0)
            mycursor.execute(sql, val)
            id_db = mycursor.lastrowid
            mydb.commit()

            data_csv = []
            for cs in reader:
                data_csv.append(cs)
            count = []
            for n in np.shape(data_csv):
                count.append(n)

            columns = int(count[0])
            row = int(count[1])
            for i in range(1, columns):
                sum = []
                for r in range(0, row):
                    a = data_csv[i][r]
                    sum.append(a)
                    mycursor = mydb.cursor()
                    sql = "INSERT INTO `TB_TR_IMPORT_DATA` (`id`, `columns`, `rows`, `value`, `type`, `source`, `date`, `doc_pdpa_data_id`, `import_file_id`) VALUES (NULL, %s, %s, %s, %s, %s, %s, %s, %s)"
                    val = ( r, i, a, 'ftp',file_name_old, date, title[r], id_db)
                    mycursor.execute(sql, val)
                    mydb.commit()
            sub.call('mv /var/pdpa/ftp/'+retxt+ ' /var/sum_file/'+randomtext+'.txt', shell=True)
            print('True')
            print(file_name_old, date, id_import, randomtext,0)
        else:
            print('false')


def read_sql(file_name):

    #RANDOM
    letters = string.ascii_lowercase
    randomtext = ''.join(random.choice(letters) for i in range(20))

    mycursor = mydb.cursor()
    sql = "SELECT * FROM TB_TR_IMPORT as im WHERE im.username = %s"
    adr = (file_name[1], )
    mycursor.execute(sql, adr)
    myresult = mycursor.fetchall()
    for x in myresult:
        id_import = x[0]

    mycursor = mydb.cursor()
    sql = "INSERT INTO `TB_TR_IMPORT_FILE` (`id`, `name`, `date`, `import_id`, `rname`, `status`) VALUES (NULL, %s, %s, %s, %s, %s)"
    val = ( file_name[0], date, id_import, randomtext,0)
    mycursor.execute(sql, val)
    id_db = mycursor.lastrowid
    mydb.commit()

    with open('/var/pdpa/ftp/'+file_name[0]) as f:
        for index, line in enumerate(f):
            sql = line.strip()
            check_title = '`id`,`columns`,`rows`,`value`,`type`,`import_file_id`,`source`,`date`,`doc_pdpa_data_id`'

            if check_title in sql:
                mycursor = mydb.cursor()
                sql = sql
                mycursor.execute(sql)
                id_db2 = mycursor.lastrowid

                mycursor = mydb.cursor()
                sql = "UPDATE TB_TR_IMPORT_DATA SET import_file_id = %s WHERE id = %s"
                val = (id_db, id_db2)
                mycursor.execute(sql, val)
                mydb.commit()

            else:
                print('false')
    
    sub.call('mv /var/pdpa/ftp/'+file_name[0]+ ' /var/sum_file/'+randomtext+'.sql', shell=True)
    print('True')
    print(file_name[0], date, id_import, randomtext,0)





while True:
    mydb = mysql.connector.connect(
    host="172.16.44.132",
    user="root",
    password="P@ssw0rdsit",
    database="DOL_PDPA"
    )
    mycursor = mydb.cursor()
    mycursor.execute("SELECT now() as date")
    myresult = mycursor.fetchall()
    for x in myresult:
        date = x[0]


    directory = 'ls -l /var/pdpa/ftp/'
    file_name_sum = read_name_file(directory)
    print(file_name_sum)

    if file_name_sum:
        for i in range(len(file_name_sum)):
            if file_name_sum[i][2] != '0':
                if 'xlsx' in file_name_sum[i][0]:
                    read_excel(file_name_sum[i])
                    print('Ex: ')
                elif 'csv' in file_name_sum[i][0]:
                    read_csv(file_name_sum[i])
                    print('Cs: ')
                elif 'txt' in file_name_sum[i][0]:
                    read_txt(file_name_sum[i])
                    print('Tx: ')
                elif 'sql' in file_name_sum[i][0]:
                    read_sql(file_name_sum[i])
                    print('Sql: ')
            else:
                print('F')
    else:
        print('none file')
    mydb.close
    time.sleep(2)
    

